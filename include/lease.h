#ifndef LEASE_H__
#define LEASE_H__
#include <stdio.h>
#include <queue.h>
#include <netinet/in.h>
#include <fsm.h>

struct lease_node {
    struct lease_node *next;
    struct in_addr ip_addr;
    char *client_hostname;
    char hardware_addr[16];
    queue *client_queue;
    fsm *Fsm;
} lease_node;

typedef struct lease {
    struct lease_node *head;
} lease;

void add_lease(lease*, struct lease_node*);
int del_lease(lease*, char*);
struct lease_node *find_lease(lease *, char*);

#endif /* LEASE_H__ */
