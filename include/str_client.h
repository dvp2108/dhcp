#ifndef STR_CLIENT_H__
#define STR_CLIENT_H__
#include <stdio.h>
#include <malloc.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stddef.h>
#include <netinet/in.h>

typedef struct str_client_node{
    void *object;
    unsigned char id[16];
    struct in_addr addr;
    struct str_client_node *next;
} str_client_node;

typedef struct str_client {
    str_client_node *head;
    pthread_mutex_t *mutex_client;
} str_client;

str_client *init_list_client();
int de_list_client(str_client*);
void add_client(str_client*, unsigned char*, void*, ssize_t);
int del_client(str_client*, unsigned char*);
int find_client(str_client*, unsigned char*, void*, ssize_t);
int set_ip_client(str_client*, unsigned char*, struct in_addr*);

#endif /* LEASE_H__ */
