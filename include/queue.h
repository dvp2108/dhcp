#ifndef QUEUE_H__
#define QUEUE_H__

#include <malloc.h>
#include <string.h>
#include <stddef.h>
#include <pthread.h>
#include <unistd.h>
#include <malloc.h>
#include <semaphore.h>

typedef struct listnode
{
    void *object;
    struct listnode *next;
} listnode;

typedef struct queue
{
    listnode *HeadPtr;
    listnode *TailPtr;
    sem_t *sem;
    pthread_mutex_t *mutex_queue;
    int NumItems;
} queue;

queue *InitQueue();
int deInitQueue(queue *);

int enqueue(queue*, void*, size_t);
int dequeue(queue*, void*, size_t);

#endif /* QUEUE_H__ */
