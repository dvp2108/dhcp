#ifndef FSM_H__
#define FSM_H__
#include <stdlib.h>
#include <malloc.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

struct transit{
    int target_state;
    int event;
    void (*func)();
    struct transit *next;
};

struct start_state{
    int base_state;
    struct start_state *next;
    struct transit *tran;
};

struct time_out{
    void (*p)();
    void *arg;
    time_t start, end;
    pthread_t pth;
};

typedef struct{
    int state_fsm;
    struct time_out time_out;
    struct start_state *head;
} fsm;

fsm *initfsm();
int deinfsm(fsm*);
int addtransition(fsm*, int, int, int, void (*p)());
void *transition(fsm*, int);
void add_timeout(fsm*, void (*p)(), void *arg);
void set_timeout(fsm*, unsigned long);

#endif /* FSM_H__ */
