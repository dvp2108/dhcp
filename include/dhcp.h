#ifndef DHCP_H__
#define DHCP_H__

#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <queue.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/poll.h>
#include <fsm.h>
#include <str_client.h>

/*
 * Server config
 */
#define DEFAULT_LEASE_TIME      60
#define MAX_LEASE_TIME          7200
#define IP_ADDRESS_SERVER       0xC0A80601
#define SIBNET_MASK             0xFFFFFF00
#define BROADCAST_ADDR          0xC0A806FF
#define IP_MIN                  0xC0A80632
#define IP_MAX                  0xC0A80664

/*
 * Packet config
 */

#define DHCP_SNAME_LEN      64
#define DHCP_FILE_LEN       128
#define DHCP_MTU_MAX        1500
#define DHCP_FIXED_NON_UDP  236
#define DHCP_FIXED_LEN      (DHCP_FIXED_NON_UDP - 20 + 8) /* fixed_non_udp - ipheader + udpheader */
#define DHCP_MAX_OPTION_LEN (DHCP_MTU_MAX - DHCP_FIXED_LEN)

struct ip_addr_node{
    struct ip_addr_node *next;
    struct in_addr addr;
    unsigned char chaddr[16];
};
typedef struct{
    struct ip_addr_node *head;
    pthread_mutex_t mutex_ip_addr;
    int num;
} ip_addr;

struct lease{
    pthread_t pth;
    queue *put_queue;
    queue *queue_client;
    fsm *fsm_client;
};

struct arg_func{
    queue *get_queue;
    queue *put_queue;
    pthread_t pth;
};

struct arg_func_pack{
    struct str_client *client;
    unsigned char chaddr[16];
    ip_addr *ip_head;
};

struct dhcp_packet {
    u_int8_t op;
    u_int8_t htype;
    u_int8_t hlen;
    u_int8_t hops;
    u_int32_t xid;
    u_int16_t secs;
    u_int16_t flags;
    struct in_addr ciaddr;
    struct in_addr yiaddr;
    struct in_addr siaddr;
    struct in_addr qiaddr;
    unsigned char chaddr[16];
    char sname [DHCP_SNAME_LEN];
    char file [DHCP_FILE_LEN];
    unsigned char options [DHCP_MAX_OPTION_LEN];
};

struct packet {
    struct dhcp_packet raw;
    unsigned packet_length;
    int packet_type;
};

/* DHCP message types */
#define DHCPDISCOVER    1
#define DHCPOFFER       2
#define DHCPREQVEST     3
#define DHCPDECLINE     4
#define DHCPACK         5
#define DHCPNAK         6
#define DHCPRELEASE     7
#define DHCPINFORM      8

#define STATE0          0
#define STATE1          1
#define STATE2          2

void *get_with_network(void*);
void *put_with_network(void*);
void *dhcp(void*);

#endif /* DHCP_H__ */
