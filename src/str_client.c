#include <str_client.h>

str_client *init_list_client()
{
    str_client *client;
    client = malloc(sizeof(str_client));
    client->mutex_client = malloc(sizeof(pthread_mutex_t));
    pthread_mutex_init(client->mutex_client, 0);
    return client;
}

int de_list_client(str_client *client)
{
    pthread_mutex_destroy(client->mutex_client);
    free(client->mutex_client);
    free(client);
    return 0;
}

void add_client(str_client *head,
        unsigned char *id,
        void *object,
        ssize_t size)
{
    str_client_node *cl;
    str_client_node *temp;
    cl = malloc(sizeof(struct str_client_node));
    pthread_mutex_lock(head->mutex_client);
    cl->object = malloc(size);
    memcpy(cl->object, object, size);
    memcpy(cl->id, id, 16);
    cl->next = NULL;

    if (head->head == NULL) {
        head->head = cl;
    } else {
        temp = head->head;
        while (temp->next != NULL) {
            temp = temp->next;
        }
        temp->next = cl;
    }
    pthread_mutex_unlock(head->mutex_client);
}

int del_client(str_client *head, unsigned char *id)
{
    str_client_node *temp, *prev;

    if (head->head == NULL)
        return 0;
    temp = head->head;
    prev = NULL;
    pthread_mutex_lock(head->mutex_client);
    if(memcmp(temp->id, id, 16) == 0) {
        head->head = temp->next;
        free(temp->object);
        free(temp);
        pthread_mutex_unlock(head->mutex_client);
        return 0;
    }
    do {
        if(memcmp(temp->id, id, 16) == 0) {
            if(prev == NULL) {
                head->head = temp->next;
            } else {
                prev = temp->next;
            }
            free(temp->object);
            free(temp);
            break;
        }
        prev = temp;
        temp = temp->next;
    } while (temp != NULL);
    pthread_mutex_unlock(head->mutex_client);
    return 0;
}

int find_client(str_client *head, 
       unsigned char *id,
       void *object,
       ssize_t size)
{
    str_client_node *temp;
    if (head->head == NULL) {
        object = NULL;
        return 0;
    } else {
        pthread_mutex_lock(head->mutex_client);
        temp = head->head;
        do {
            if(!memcmp(temp->id, id, 16)) {
                memcpy(object, temp->object, size);
                pthread_mutex_unlock(head->mutex_client);
                return 1;
            }
            temp = temp->next;
        } while (temp != NULL);
    }
    pthread_mutex_unlock(head->mutex_client);
    return 0;
}

int set_ip_client (str_client *head, 
       unsigned char *id,
       struct in_addr *addr)
{
    str_client_node *temp;
    if (head->head == NULL) {
        return 1;
    } else {
        pthread_mutex_lock(head->mutex_client);
        temp = head->head;
        do {
            if (!memcmp(temp->id, id, 16)) {
                temp->addr.s_addr = addr->s_addr;
                pthread_mutex_unlock(head->mutex_client);
                return 0;
            }
            temp = temp->next;
        } while (temp != NULL);
    }
    pthread_mutex_unlock(head->mutex_client);
    return 1;
}

