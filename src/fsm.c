#include <fsm.h>

void *timeout(void *arg) 
{
    fsm *Fsm = (fsm*)arg;
    void (*p)(void *) = Fsm->time_out.p;
    while (time(NULL) < Fsm->time_out.end) {
        printf("timeout start %i\n", Fsm->time_out.end - time(NULL));
        sleep(Fsm->time_out.end - time(NULL));
    }
    printf("timeout end\n");
    p(Fsm->time_out.arg);
}

void add_timeout(fsm *Fsm, void (*p)(), void *arg)
{
    Fsm->time_out.p = p;
    Fsm->time_out.arg = arg;
}

void set_timeout(fsm *Fsm, unsigned long time_set)
{
    Fsm->time_out.start = time(NULL);
    Fsm->time_out.end = Fsm->time_out.start + time_set;
    pthread_cancel(Fsm->time_out.pth);
    pthread_create(&(Fsm->time_out.pth), NULL, &timeout, (void*)Fsm);
}

fsm *initfsm()
{
    fsm *Fsm;
    Fsm = malloc(sizeof(fsm));
    Fsm->head = NULL;
    Fsm->time_out.end = time(NULL) + 3600;
    pthread_create(&(Fsm->time_out.pth), NULL, &timeout, (void*)Fsm);
    return Fsm;
}

int deinfsm(fsm *Fsm)
{
    if (Fsm->head != NULL) {
        struct start_state *state;
        struct transit *tran;
        do {
            state = Fsm->head;
            do {
                tran = state->tran;
                state->tran = tran->next;
                free(tran);
            } while (state->tran != NULL);
            Fsm->head = state->next;
            free(state);
        } while (Fsm->head != NULL);
    } else {
        free(Fsm);
    }
    free(Fsm);
    return 0;
}

int addtransition(fsm* Fsm, int base_state, int target_state,
        int event, void (*p)())
{
    struct start_state *state;
    struct transit *tran;

    tran = malloc(sizeof(struct transit));
    tran->target_state = target_state;
    tran->event = event;
    tran->func = p;
    tran->next = NULL;

    if (Fsm->head == NULL) {
        state = malloc(sizeof(struct start_state));
        state->base_state = base_state;
        state->next = NULL;
        state->tran = tran;
        Fsm->head = state;
        Fsm->state_fsm = base_state;
    } else {
        state = Fsm->head;
        while(state->next != NULL){
            if (state->base_state == base_state) {
                struct transit *temp;
                temp = state->tran;
                while (temp->next != NULL);
                temp->next = tran;
                return 0;
            }
            state = state->next;
        }
        struct start_state *temp;
        temp = malloc(sizeof(struct start_state));
        temp->base_state = base_state;
        temp->next = NULL;
        temp->tran = tran;
        state->next = temp;
    }
    return 0;
}

void *transition (fsm *Fsm, int event)
{
    struct transit *tran;
    struct start_state *state;
    state = Fsm->head;

    do {
        if (Fsm->state_fsm == state->base_state) {
            tran = state->tran;
            do {
                if (tran->event == event) {
                    printf("%i -> %i \n", Fsm->state_fsm, tran->target_state);

                    Fsm->state_fsm = tran->target_state;
                    return tran->func;
                }
                tran = tran->next;
            } while (tran != NULL);
        }
        state = state->next;
    } while (state != NULL);
    return NULL;
}
