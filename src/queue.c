#include <queue.h>

queue *InitQueue()
{
    queue *Queue;
    Queue = malloc(sizeof(queue));
    Queue->HeadPtr = NULL;
    Queue->TailPtr = NULL;
    Queue->sem = malloc(sizeof(sem_t));
    Queue->mutex_queue = malloc(sizeof(pthread_mutex_t));
    pthread_mutex_init(Queue->mutex_queue, 0);
    sem_init(Queue->sem, 0, 0);
    Queue->NumItems = 0;
    return Queue;
}

int deInitQueue(queue *Queue)
{
    pthread_mutex_destroy(Queue->mutex_queue);
    free(Queue->mutex_queue);
    sem_destroy(Queue->sem);
    free(Queue->sem);
    free(Queue);
    return 0;
}

int enqueue(queue *Queue,
        void *Object,
        size_t Size)
{
    pthread_mutex_lock(Queue->mutex_queue);
    listnode *lnode;
    void *object;
    lnode = malloc(sizeof(listnode));
    object = malloc(Size);

    memcpy(object, Object, Size);
    lnode->object = object;
    lnode->next = NULL;
    if( Queue->NumItems == 0 ) {
        Queue->HeadPtr = lnode;
        Queue->TailPtr = lnode;
    }
    else {
        Queue->TailPtr->next = lnode;
        Queue->TailPtr = lnode;
    }
    ++Queue->NumItems;
    pthread_mutex_unlock(Queue->mutex_queue);
    sem_post(Queue->sem);
    return 0;
}

int dequeue(queue *Queue, 
        void *Object,
        size_t Size)
{
    sem_wait( Queue->sem );
    pthread_mutex_lock(Queue->mutex_queue);
    if( Queue->NumItems > 0 ) {
        listnode *lnode;
        lnode = Queue->HeadPtr;
        memcpy(Object, lnode->object, Size);

        Queue->HeadPtr = lnode->next;
        free(lnode);

        --Queue->NumItems;
        if( Queue->NumItems == 0 ) {
            Queue->TailPtr = 0;
        }
    }
    pthread_mutex_unlock(Queue->mutex_queue);
    return 0;
}
