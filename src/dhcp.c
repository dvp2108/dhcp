#include <dhcp.h>

unsigned char get_option_byte(unsigned char *ptr,
        unsigned int offset)
{
    return *(ptr + offset);
}
unsigned short get_option_short(unsigned char *ptr,
        unsigned int offset)
{
    unsigned short value;
    value = (unsigned short) *(ptr + offset) << 8;
    value |= (unsigned short) *(ptr + offset + 1);

    return value;
}
unsigned long get_option_long(unsigned char *ptr,
        unsigned int offset)
{
    unsigned long value;
    value = (unsigned long) *(ptr + offset) << 24;
    value |= (unsigned long) *(ptr + offset + 1) << 16;
    value |= (unsigned long) *(ptr + offset + 2) << 8;
    value |= (unsigned long) *(ptr + offset + 3);

    return value;
}
void dhcp_option(unsigned char *options,
        unsigned int offset,
        unsigned char opt_type,
        unsigned char opt_len)
{
    *(options + offset) = opt_type;
    *(options + offset+1) = opt_len;
}
void dhcp_option_byte(unsigned char *options, 
        unsigned int offset, 
        unsigned char value)
{
    *(options + offset) = value;
}
void dhcp_option_short(unsigned char *options,
        unsigned int offset,
        unsigned short value)
{
    *(options + offset) = (value & 0xFF00) >> 8;
    *(options + offset+1) = value & 0x00FF;
}
void dhcp_option_long(unsigned char *options,
        unsigned int offset,
        unsigned long value)
{
    *(options + offset) = (value & 0xFF000000) >> 24;
    *(options + offset+1) = (value & 0x00FF0000) >> 16;
    *(options + offset+2) = (value & 0x0000FF00) >> 8;
    *(options + offset+3) = value & 0x000000FF;
}

void *get_with_network(void *arg)
{
    struct arg_func *argum = (struct arg_func*)arg;
    int sock;
    struct sockaddr_in addr;

    struct dhcp_packet raw;
    struct packet client_packet;
    unsigned bytes_read;
    int idx;

    sock = socket(AF_INET, SOCK_DGRAM, 0);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(67);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind (sock, (struct sockaddr *)&addr, sizeof(struct sockaddr)) < 0) {
        perror ("ERROR BIND");
        return 0;
    }
    while (1) {
        bytes_read = recv(sock, &raw, sizeof(struct dhcp_packet), 0);
        client_packet.raw = raw;
        client_packet.packet_length = bytes_read;
        enqueue(argum->get_queue, &client_packet, sizeof(struct packet));
    }
}

void *put_with_network(void *arg)
{
    struct arg_func *argum = (struct arg_func*)arg;
    int sock;
    struct sockaddr_in addr;
    int tm = 1;
    struct packet client_packet;

    sock = socket (AF_INET, SOCK_DGRAM, 0);
    setsockopt (sock, SOL_SOCKET, SO_BROADCAST, (char*)&tm, sizeof(int));

    addr.sin_family = AF_INET;
    addr.sin_port = htons(68);
    addr.sin_addr.s_addr = htonl(BROADCAST_ADDR);

    while (1) {
        dequeue (argum->put_queue, &client_packet, sizeof(struct packet));
        sendto (sock, &(client_packet.raw), client_packet.packet_length, 0, 
                (struct sockaddr*)&addr, sizeof(addr));
        printf("dequeue %i\n", client_packet.packet_length);
    }
}
// Просто добавление адреса
int add_ip(ip_addr *ip_head, struct in_addr *addr, unsigned char *chaddr)
{
    struct ip_addr_node *ip_node = malloc(sizeof(struct ip_addr_node));
    ip_node->addr.s_addr = addr->s_addr;
    ip_node->next = NULL;
    memcpy(&ip_node->chaddr, chaddr, 16);
    struct ip_addr_node *temp;
    if (addr->s_addr > IP_MAX) {
        free (ip_node);
        return 1;
    }
    pthread_mutex_lock(&ip_head->mutex_ip_addr);
    if (ip_head->head == NULL) {
        ip_head->head = ip_node;
        pthread_mutex_unlock(&ip_head->mutex_ip_addr);
        return 0;
    } else {
        temp = ip_head->head;
        if (temp->addr.s_addr > addr->s_addr) {
            if (addr->s_addr < IP_MIN) {
                free (ip_node);
                pthread_mutex_unlock(&ip_head->mutex_ip_addr);
                return 1;
            }
            ip_node->next = ip_head->head;
            ip_head->head = ip_node;
            pthread_mutex_unlock(&ip_head->mutex_ip_addr);
            return 0;
        }
        do {
            if (temp->addr.s_addr == addr->s_addr) {
                free (ip_node);
                pthread_mutex_unlock(&ip_head->mutex_ip_addr);
                return 1;
            }
            if (temp->next == NULL) {
                temp->next = ip_node;
                pthread_mutex_unlock(&ip_head->mutex_ip_addr);
                return 0;
            }
            if ((temp->addr.s_addr < addr->s_addr) && 
                    (temp->next->addr.s_addr > addr->s_addr)) {
                ip_node->next = temp->next;
                temp->next = ip_node;
                pthread_mutex_unlock(&ip_head->mutex_ip_addr);
                return 0;
            }
            temp = temp->next;
        } while (temp != NULL);
    }
}

int del_ip (ip_addr *ip_head, char *chaddr)
{
    struct ip_addr_node *temp, *prev;
    pthread_mutex_lock(&ip_head->mutex_ip_addr);
    if (ip_head->head == NULL){
        pthread_mutex_unlock(&ip_head->mutex_ip_addr);
        return 1;
    }
    temp = ip_head->head;
    prev = NULL;
    do {
        if (memcmp(&temp->chaddr, chaddr, 16) == 0) {
            printf("delete ip %i\n", temp->addr);
            if (prev == NULL)
                ip_head->head = temp->next;
            else
                prev = temp->next;
            free(temp);
            break;
        }
        prev = temp;
        temp = temp->next;
    } while (temp != NULL);
    pthread_mutex_unlock(&ip_head->mutex_ip_addr);
    return 0;
}
// Поиск по ip адресу
int find_ip (ip_addr *ip_head, struct in_addr *addr, unsigned char *chaddr)
{
    struct ip_addr_node *ip_node;
    struct ip_addr_node *temp;
    pthread_mutex_lock(&ip_head->mutex_ip_addr);
    if (addr->s_addr != 0) {
        if (!(addr->s_addr <= IP_MAX)) {
            if (!(addr->s_addr >= IP_MIN)) {
                pthread_mutex_unlock(&ip_head->mutex_ip_addr);
                return -1;
            }
        }
    }

    if (ip_head->head == NULL) {
        if (addr->s_addr == 0) {
            addr->s_addr = IP_MIN;
            pthread_mutex_unlock(&ip_head->mutex_ip_addr);
            return 0;
        } else {
            pthread_mutex_unlock(&ip_head->mutex_ip_addr);
            return 0;
        }
    }

    temp = ip_head->head;
    do {
        if (addr->s_addr == 0) {
            if (temp->addr.s_addr < IP_MAX) {
                if (temp->next == NULL) {
                    addr->s_addr = temp->addr.s_addr + 1;
                    pthread_mutex_unlock(&ip_head->mutex_ip_addr);
                    return 0;
                }
                if ((temp->addr.s_addr + 1) != temp->next->addr.s_addr) {
                    addr->s_addr = temp->addr.s_addr + 1;
                    pthread_mutex_unlock(&ip_head->mutex_ip_addr);
                    return 0;
                }
            } else {
                pthread_mutex_unlock(&ip_head->mutex_ip_addr);
                return -1;
            }
        } else {
            if (temp->addr.s_addr == addr->s_addr) {
                if(memcmp(temp->chaddr, chaddr, 16) == 0) {
                    pthread_mutex_unlock(&ip_head->mutex_ip_addr);
                    return 1;
                } else {
                    pthread_mutex_unlock(&ip_head->mutex_ip_addr);
                    return -1;
                }
            };
        }
        temp = temp->next;
    } while (temp != NULL);
    pthread_mutex_unlock(&ip_head->mutex_ip_addr);
    return 0;
}

void delclient(void *arg)
{
    struct arg_func_pack *arg_cl = (struct arg_func_pack*)arg;
    struct lease client_lease;
    find_client (arg_cl->client, arg_cl->chaddr, &client_lease, sizeof(struct lease));
    deInitQueue(client_lease.queue_client);
    deinfsm(client_lease.fsm_client);
    del_client(arg_cl->client, arg_cl->chaddr);
    del_ip(arg_cl->ip_head, arg_cl->chaddr);
    free(arg);
    printf("delete client\n");
}

void discover(void *arg)
{
    struct arg_func_pack *arg_pack = (struct arg_func_pack*)arg;
    struct packet raw;
    struct lease client_lease;
    struct in_addr addr;
    int idx;
    int status;
    addr.s_addr = 0;
    printf("diskover \n");

    find_client(arg_pack->client, arg_pack->chaddr, &client_lease, sizeof(struct lease));

    dequeue(client_lease.queue_client, &raw, sizeof(struct packet));

    for (idx = 4; idx < DHCP_MAX_OPTION_LEN; ++idx) {
        if (raw.raw.options[idx] == 0xFF) {
            break;
        }
        if (raw.raw.options[idx] == 50) {
            idx += 2;
            addr.s_addr = get_option_long(raw.raw.options, idx);
            break;
        } else {
            ++idx;
            idx += raw.raw.options[idx];
        }
    }

    status = find_ip(arg_pack->ip_head, &addr, raw.raw.chaddr);
    if (status == -1) {
        addr.s_addr = 0x00;
        find_ip(arg_pack->ip_head, &addr, raw.raw.chaddr);
    }

    raw.raw.op = 2;
    raw.raw.yiaddr.s_addr = htonl(addr.s_addr);

    idx = 4;
    dhcp_option(raw.raw.options, idx, 53, 1);
    idx += 2;
    dhcp_option_byte(raw.raw.options, idx, DHCPOFFER);
    idx += 1;
    dhcp_option(raw.raw.options, idx, 54, 4);
    idx += 2;
    dhcp_option_long(raw.raw.options, idx, IP_ADDRESS_SERVER);
    idx += 4;
    dhcp_option(raw.raw.options, idx, 51, 4);
    idx += 2;
    dhcp_option_long(raw.raw.options, idx, DEFAULT_LEASE_TIME);
    idx += 4;
    dhcp_option(raw.raw.options, idx, 1, 4);
    idx += 2;
    dhcp_option_long(raw.raw.options, idx, SIBNET_MASK);
    idx += 4;
    raw.raw.options[idx] = 255;
    idx += 1;
    raw.packet_length = DHCP_FIXED_NON_UDP + idx;

    enqueue(client_lease.put_queue, &raw, sizeof(struct packet));
    set_timeout(client_lease.fsm_client, 10);
    printf("diskover %i\n", addr.s_addr);
    free (arg_pack);
}

void reqvest(void *arg)
{
    struct arg_func_pack *arg_pack = (struct arg_func_pack*)arg;
    struct packet raw;
    struct lease client_lease;
    struct in_addr addr;
    int idx;
    int status;
    addr.s_addr = 0;
    find_client(arg_pack->client, arg_pack->chaddr, &client_lease, sizeof(struct lease));
    dequeue(client_lease.queue_client, &raw, sizeof(struct packet));

    for (idx = 4; idx < DHCP_MAX_OPTION_LEN; ++idx) {
        if (raw.raw.options[idx] == 0xFF) {
            break;
        }
        if (raw.raw.options[idx] == 50) {
            idx += 2;
            addr.s_addr = get_option_long(raw.raw.options, idx);
            idx += 3;
        } else {
            ++idx;
            idx += raw.raw.options[idx];
        }
    }

    if (raw.raw.ciaddr.s_addr != 0) {
        addr.s_addr = ntohl(raw.raw.ciaddr.s_addr);
        printf("htonl %i\n", addr.s_addr);
    }

    status = find_ip(arg_pack->ip_head, &addr, raw.raw.chaddr);
    if (status == -1) {
        return;
    } else if (status == 0){
        add_ip(arg_pack->ip_head, &addr, raw.raw.chaddr);
    }

    raw.raw.yiaddr.s_addr = htonl(addr.s_addr);
    set_ip_client(arg_pack->client, arg_pack->chaddr, &addr);

    idx = 4;
    dhcp_option(raw.raw.options, idx, 53, 1);
    idx += 2;
    dhcp_option_byte(raw.raw.options, idx, DHCPACK);
    idx += 1;
    dhcp_option(raw.raw.options, idx, 54, 4);
    idx += 2;
    dhcp_option_long(raw.raw.options, idx, IP_ADDRESS_SERVER);
    idx += 4;
    dhcp_option(raw.raw.options, idx, 51, 4);
    idx += 2;
    dhcp_option_long(raw.raw.options, idx, DEFAULT_LEASE_TIME); 
    idx += 4;
    dhcp_option(raw.raw.options, idx, 1, 4);
    idx += 2;
    dhcp_option_long(raw.raw.options, idx, SIBNET_MASK);
    idx += 4;
    raw.raw.options[idx] = 255;
    idx += 1;
    raw.packet_length = DHCP_FIXED_NON_UDP + idx;
    set_timeout(client_lease.fsm_client, DEFAULT_LEASE_TIME);
    printf("reqvest\n");
    enqueue(client_lease.put_queue, &raw, sizeof(struct packet));
    free(arg_pack);
}

void decline(void *client)
{
    delclient(client);
}

void release(void *client)
{
    delclient(client);
}


void fsm_table(fsm *Fsm)
{
    addtransition(Fsm, STATE0, STATE1, DHCPDISCOVER, &discover);
    addtransition(Fsm, STATE0, STATE2, DHCPREQVEST, &reqvest);
    addtransition(Fsm, STATE1, STATE2, DHCPREQVEST, &reqvest);
    addtransition(Fsm, STATE2, STATE2, DHCPREQVEST, &reqvest);
    addtransition(Fsm, STATE2, STATE0, DHCPDECLINE, &decline);
    addtransition(Fsm, STATE2, STATE0, DHCPRELEASE, &release);
}

void *dhcp(void *arg)
{
    struct arg_func *argum = (struct arg_func*)arg;
    struct packet client_packet;
    int idx;
    int num = 0;

    str_client *client;

    void *(*func)(void *client);
    void *p;
    struct lease lease_client;
    struct arg_func_pack *arg_pack;
    struct arg_func_pack *arg_timeout;
    ip_addr ip_head;
    ip_head.head = NULL;
    pthread_mutex_init(&ip_head.mutex_ip_addr, 0);

    client = init_list_client();

    while (1) {
        dequeue (argum->get_queue, &client_packet, sizeof(struct packet));
        for (idx = 4; idx < DHCP_MAX_OPTION_LEN; ++idx) {
            if (client_packet.raw.options[idx] == 0xFF) {
                break;
            }
            if (client_packet.raw.options[idx] == 53) {
                idx += 2;
                client_packet.packet_type = client_packet.raw.options[idx];
            } else if (client_packet.raw.options[idx] == 54) {
                idx += 2;
                if (get_option_long(client_packet.raw.options, idx) != IP_ADDRESS_SERVER);
                idx += 3;
            } else {
                ++idx;
                idx += client_packet.raw.options[idx];
            }
        }

        if (!find_client (client, client_packet.raw.chaddr, &lease_client, sizeof(lease_client))) {
            if (client_packet.packet_type == DHCPDISCOVER ||
                    client_packet.packet_type == DHCPREQVEST) {
                lease_client.queue_client = InitQueue();
                lease_client.fsm_client = initfsm();
                lease_client.put_queue = argum->put_queue;
                fsm_table(lease_client.fsm_client);

                arg_pack = malloc(sizeof(struct arg_func_pack));
                arg_pack->client = client;
                arg_pack->ip_head = &ip_head;
                memcpy(&arg_pack->chaddr, &client_packet.raw.chaddr, 16);

                arg_timeout = malloc(sizeof(struct arg_func_pack));
                arg_timeout->client = client;
                arg_timeout->ip_head = &ip_head;
                memcpy(&arg_timeout->chaddr, &client_packet.raw.chaddr, 16);
                add_timeout(lease_client.fsm_client, &delclient, (void*)arg_timeout);

                enqueue(lease_client.queue_client, &client_packet, sizeof(struct packet));
                add_client(client, client_packet.raw.chaddr, &lease_client, sizeof(struct lease));
                func = transition(lease_client.fsm_client, client_packet.packet_type);
                if(func != NULL) {
                    pthread_create (&(lease_client.pth), NULL, func, (void*)arg_pack);
                }
            } else {
                printf("NON DHCPDISCOVER || DHCPREQVEST\n");
            }
        } else {
            arg_pack = malloc(sizeof(struct arg_func_pack));
            arg_pack->client = client;
            arg_pack->ip_head = &ip_head;
            memcpy(&arg_pack->chaddr, &client_packet.raw.chaddr, 16);
            enqueue(lease_client.queue_client, &client_packet, sizeof(struct packet));
            func = transition(lease_client.fsm_client, client_packet.packet_type);
            if(func != NULL) {
                pthread_create (&(lease_client.pth), NULL, func, (void*)arg_pack);
            }
        }
    }
}
