#include <project.h>
/* Добавлять больше не чего не надо */

int main(int argc, char **argv)
{
    pthread_t pth_get_network;
    pthread_t pth_put_network;
    pthread_t pth_dhcp;

    queue *get_queue;
    queue *put_queue;

    struct arg_func arg;

    get_queue = InitQueue();
    put_queue = InitQueue();

    arg.get_queue = get_queue;
    arg.put_queue = put_queue;

    pthread_create (&pth_get_network, NULL, &get_with_network, (void*)&arg);
    pthread_create (&pth_dhcp, NULL, &dhcp, (void*)&arg);
    pthread_create (&pth_put_network, NULL, &put_with_network, (void*)&arg);

    pthread_join (pth_get_network, NULL);
    pthread_join (pth_dhcp, NULL);
    pthread_join (pth_put_network, NULL);

    deInitQueue (get_queue);
    deInitQueue (put_queue);
    return 0;
}
