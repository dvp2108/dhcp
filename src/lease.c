#include <lease.h>

void add_lease(lease *head, struct lease_node *new_lease)
{
    struct lease_node *temp;
    if (head->head == NULL) {
        head->head = new_lease;
        new_lease->next = NULL;
    } else {
        temp = head->head;
        while (temp->next !=NULL)
            temp = temp->next;
        new_lease->next = NULL;
        temp->next = new_lease;
    }
}

int del_lease(lease *head, char *hardware_addr)
{
    struct lease_node *temp, *prev;
    if (head->head == NULL)
        return 0;
    temp = head->head;
    prev = NULL;
    do {
        if(memcmp(&temp->ip_addr, &hardware_addr, 16)) {
            if(prev == NULL) {
                head->head = temp->next;
            } else {
                prev = temp->next;
            }
            free(temp);
            break;
        }
        prev = temp;
        temp = temp->next;
    } while (temp != NULL);
    return 0;
}

struct lease_node *find_lease(lease *head, char *hardware_addr)
{
    struct lease_node *temp;
    if (head->head == NULL) {
        return NULL;
    } else {
        temp = head->head;
        do {
            if(memcmp(&temp->hardware_addr, &hardware_addr, 16)) {
                return temp;
            }
            temp = temp->next;
        } while (temp != NULL);
    }
    return NULL;
}
