CC=gcc
INCLUDE:=-Iinclude/
MAIN:=main
BINARY:=./build/project
BIN_LIB:=libqueue.so
BIN_FSM:=libfsm.so
BIN_DHCP:=libdhcp.so
BIN_CLIENT:=libstrclient.so
LIB:=-lqueue -lpthread -lfsm -ldhcp -lstrclient

all: $(BINARY) 
$(BINARY): $(MAIN).o $(BIN_LIB) $(BIN_FSM) $(BIN_DHCP) $(BIN_CLIENT)
		$(CC) -o $(BINARY) main.o -L. $(LIB) -Wl,-rpath,.

main.o: src/$(MAIN).c
		$(CC) -c src/$(MAIN).c $(INCLUDE)

$(BIN_LIB): queue.o
		$(CC) -shared -o $(BIN_LIB) queue.o

queue.o: src/queue.c
		$(CC) -c -fPIC src/queue.c $(INCLUDE)

$(BIN_FSM): fsm.o
		$(CC) -shared -o $(BIN_FSM) fsm.o

fsm.o: src/fsm.c
		$(CC) -c -fPIC src/fsm.c $(INCLUDE)

$(BIN_DHCP): dhcp.o
		$(CC) -shared -o $(BIN_DHCP) dhcp.o

dhcp.o: src/dhcp.c
		$(CC) -c -fPIC src/dhcp.c $(INCLUDE)

$(BIN_CLIENT): str_client.o
		$(CC) -shared -o $(BIN_CLIENT) str_client.o

str_client.o: src/str_client.c
		$(CC) -c -fPIC src/str_client.c $(INCLUDE)

clean:
		rm *.o *.so
		rm $(BINARY)
